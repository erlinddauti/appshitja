import React,{useState} from 'react'
// import './App.css';
import NavigationBar from './components/NavigationBar';
import Welcome from './components/Welcome';
import {Container,Row,Col} from 'react-bootstrap'
import Footer from './components/Footer'
import AddArticle from './components/AddArticle';
import ArticleLists from './components/ArticleLists';
import {BrowserRouter as Router,Switch,Route} from 'react-router-dom'


function App() {
  
  const marginTop={
marginTop:"20px"
  };


  return (
    <Router >
      <NavigationBar/>
          <Container>
            <Row>
              <Col lg={12} style={marginTop}>
               
               <Switch>
               <Route path="/add">
            <AddArticle />
          </Route>
                 
          <Route path="/lists" component={ArticleLists}>
         
          </Route>
          <Route  path="/edit/:id">
            <AddArticle />
          </Route>
          <Route exact  path="/">
            <Welcome />
          </Route>

               </Switch>
              </Col>
            </Row>
            
          </Container>
          <Footer />
    </Router>
  );
}

export default App;
