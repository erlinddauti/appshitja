import React, { Component } from 'react'
import { Card,Table,Button,ButtonGroup } from 'react-bootstrap'
import axios from 'axios'
import {Link} from 'react-router-dom'
import MyToast from './myToast'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faEdit, faTrash, } from '@fortawesome/free-solid-svg-icons';
import AddArticle from './AddArticle'

export class ArticleLists extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             artikulli:[],
             id:'',
             cmimi:'',
             sasia:'',
             
             vlera:'',
        }
      
    }
    
    
UpdateArtikulli(id){
    console.log(id)
    this.props.history.push('/edit'+id)
}

    // artikulliEdit=(id)=>{
    //   this.props.history.push(`/edit/${id}`)
    // }
    
    componentDidMount() {
        // debugger 
        axios.get("https://localhost:44362/api/artikulli")
        .then(response=>response.data)
        .then((data)=>{
            this.setState({artikulli:data})
        })
    }

    deleteArtikulli=(artikulliID)=>{
        axios.delete("https://localhost:44362/api/artikulli/"+artikulliID)
        
        .then(res => {
        if(res.data!=null){
            this.setState({"show":true});
            setTimeout(() => this.setState({"show":false}), 3000);
            this.setState({
                artikulli:this.state.artikulli.filter(artikull=>artikull.id !== artikulliID)
            });
        }  else {
            this.setState({"show":false});
        }
        });
        
    }

    render() {
        return (
            <div>
            <div style={{"display":this.state.show ? "block" : "none"}}>
          <MyToast show = {this.state.show} message = {"Artikulli u fshi me sukses."} type = {"danger"}/>
             </div>
              <Card className={"border border-dark bg-dark text-white"}>
               <Card.Header>
                   Lista e artikujve
               </Card.Header>
               <Card.Body>               
        <Table bordered hover striped variant="dark">
           <thead>
    <tr>
      <th>Emri</th>
      <th>Cmimi</th>
      <th>Sasia</th>
      <th>Vlera</th>
      <th>Edit/Delete</th>
    </tr>
  </thead>
  <tbody>

  {this.state.artikulli.length===0 ?
    <tr align="center">
      <td colSpan="6"> No Article Available</td>
    </tr> :
    this.state.artikulli.map((artikull)=>(
           <tr key={artikull.id}>
            <td>{artikull.emri}</td>
            <td>{artikull.cmimi}</td>
            <td>{artikull.sasia}</td>
            <td>{artikull.vlera}</td>
            <ButtonGroup>
            <Link  to={"edit/"+artikull.id}  onClick={()=>this.UpdateArtikulli(artikull.id)} className="btn btn-lg btn-outline-primary" ><FontAwesomeIcon icon={faEdit} /></Link>{' '}
                <Button size="lg" className="btn btn-lg btn-outline-danger"  onClick={this.deleteArtikulli.bind(this,artikull.id)} style={{marginLeft:"5px"}} >  <FontAwesomeIcon icon={faTrash} /> </Button>
            </ButtonGroup>
        </tr>

    ))
    }
{/*  */}
    </tbody>
                   </Table>
               </Card.Body>
           </Card>
            </div>
           
        )
    }
}
export default ArticleLists
