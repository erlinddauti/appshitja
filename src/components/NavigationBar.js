import React, { Component } from 'react'
import {Navbar,Nav} from 'react-bootstrap'
import Logo from '../logo123.png'
import {Link} from 'react-router-dom'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faPlus, faList, faPlusCircle,} from '@fortawesome/free-solid-svg-icons';


class NavigationBar extends Component {
    render() {
        return (
            <div>
                 <Navbar bg="dark" variant="dark">
                     <Link to="/" className="navbar-brand">

                    <img src={Logo}  width="50px" height="25" alt="logo"/>  ShitjaApp
                     </Link>
                
                 <Nav className="mr-auto">
     
                 <Link to="/add" className="nav-link" > <FontAwesomeIcon icon={faPlusCircle} /> {'  '} Artikuj</Link>
                  <Link to="/lists" className="nav-link" > <FontAwesomeIcon icon={faList} /> {'  ' } Lista e artikujve</Link>
                      </Nav>
            
                 </Navbar>
             
                
            </div>
        )
    }
}

export default NavigationBar
